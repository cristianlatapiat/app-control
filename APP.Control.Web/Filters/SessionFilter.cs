﻿using APP.Control.Web.Controllers;

_{{

using APP.Control.Web.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Services.Protocols;

namespace APP.Control.Web.Filters
{
    public class SessionFilter: ActionFilterAttribute
    {
        private LoginViewModel user;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);

                user = (LoginViewModel)HttpContext.Current.Session["user"];
                if (user == null)
                {
                   
                    if (filterContext.Controller is AccountController == false)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "Account", 
                            action = "Login"        
                        }));
                    }
                }
            }
            catch (Exception)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Login"
                }));
            }
          
        }

    }
}