﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(APP.Control.Web.Startup))]
namespace APP.Control.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
